#+TITLE: best practices

* GF LREC tutorial
available [[http://www.grammaticalframework.org/doc/gf-lrec-2010.pdf][here]].

** Suggested order for proceeding with a language
1. ResMar: parameter types needed for nouns
2. CatMar: lincat N
3. ParadigmsMar: some regular noun paradigms
4. LexiconMar: some words that the new paradigms cover
5. (1.-4.) for V, maybe with just present tense
6. ResMar: parameter types needed for Cl, CN, Det, NP, Quant, VP
7. CatMar: lincat Cl, CN, Det, NP, Quant, VP
8. NounMar: lin DetCN, DetQuant
9. VerbMar: lin UseV
10. SentenceMar: lin PredVP

** Diagnosis methods along the way
Make sure you have a compilable LangMar at all times!
: pg -missing
 to check which functions are missing.
: gr -cat=C | l -table
 to test category C

** Regression testing with a treebank
Build and maintain a treebank: a set of trees with their
linearizations:
1. Create a file test.trees with just trees, one by line.
2. Linearize each tree to all forms, possibly with English for
   comparison
#+BEGIN_SRC sh
> i english/LangEng.gf
> i marathi/LangMar.gf
> rf -lines -tree -file=trs | l -all -treebank | wf test.treebank
#+END_SRC

3. Create a gold standard gold.treebank from test.treebank by manually
   correcting the Marathi linearizations.
4. Compare with the Unix command diff test.treebank gold.treebank
5. Rerun (2.) and (4.) after every change in concrete syntax; extend
   the tree set and the gold standard after every new implemented
   function.

** Compiling the library
The current development library sources are in GF/lib/src.
- Use =make= in this directory to compile the libraries.
- Use =runghc Make lang api langs=Mar= to compile just the language
  =Mar=.

from tools.tools import render_template
from tools.besch.besch_maker import make_paradigms_json
from tools.besch.mk_template import make_template_json
from tools.besch.mk_irreg_besch import make_irreg_besch_json
from sys import argv

if __name__ == "__main__":
    subcommand = argv[1].lower()
    if subcommand == "paradigms-make":
        print(make_paradigms_json(argv[2]))
    elif subcommand == "paradigms-template":
        print(make_template_json(argv[2]))
    elif subcommand == "irreg-besch":
        print(make_irreg_besch_json(argv[2]))
    else:
        raise Exception("unknown subcommand")

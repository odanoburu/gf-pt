#!/bin/sh

SCRIPTDIR="$(dirname "$0")"
ROOTDIR=$SCRIPTDIR/../../

# generate meta-template
python $ROOTDIR/main.py paradigms-template $SCRIPTDIR/paradigms.forms > $SCRIPTDIR/forms.json
yasha -v $SCRIPTDIR/forms.json $SCRIPTDIR/BeschPor.gf.template.template -o $SCRIPTDIR/BeschPor.gf.template

# generate BeschPor
python $ROOTDIR/main.py paradigms-make $SCRIPTDIR/paradigms.besch > $SCRIPTDIR/besch.json
yasha -v $SCRIPTDIR/besch.json $SCRIPTDIR/BeschPor.gf.template -o $SCRIPTDIR/BeschPor.gf

# generate IrregPor
cat $SCRIPTDIR/verbs.besch | tr -s ' '  > $SCRIPTDIR/verbs.besch.clean
python $ROOTDIR/main.py irreg-besch $SCRIPTDIR/verbs.besch.clean > $SCRIPTDIR/verbs.json
yasha -v $SCRIPTDIR/verbs.json $SCRIPTDIR/IrregPorAbs.gf.template -o $SCRIPTDIR/IrregPorAbs.gf
yasha -v $SCRIPTDIR/verbs.json $SCRIPTDIR/IrregPor.gf.template -o $SCRIPTDIR/IrregPor.gf

if [ "$1" = "clean" ]; then
    rm $SCRIPTDIR/forms.json $SCRIPTDIR/besch.json $SCRIPTDIR/BeschPor.gf.template $SCRIPTDIR/verbs.json $SCRIPTDIR/verbs.besch.clean
fi

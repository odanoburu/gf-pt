# reads the paradigm.form file, and builds a .json structure with its
# data, which is then fed to the metatemplate by yasha, yielding the
# template.

from sys import argv
import json


def read_forms(flines):
    forms = []
    forms.append(read_form(next(flines), "VI"))  # infn
    forms.append(read_form(next(flines), "VI"))  # ger
    forms.append(read_form(next(flines), "VI"))  # part
    for line in flines:
        forms.append(read_form(line, "VPB"))
    return forms


def read_form(line, label):
    [form, mood, number, person, present] = line.split()
    params = " ".join(filter(lambda x: x.strip() != "None",
                             [mood, number, person]))
    tree = "{} {}".format(label, form) if params == "" else "{} ({} {})".format(label, form, params)
    key = form.lower() if params == "" else "{}_{}".format(form.lower(), params.lower().replace(' ', '_'))
    is_present = "" if present == "True" else " --# notpresent"
    return {"tree": tree, "key": key, "is_present": is_present}


def make_template_json(in_fp):
    with open(in_fp, mode='r', encoding="utf8") as fh:
        fs = read_forms(fh)
        return json.dumps({"forms": fs},
                          ensure_ascii=False,
                          check_circular=True)

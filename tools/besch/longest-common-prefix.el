(require 'cl-lib)

(defun my-longest-common-prefix-lines (start end)
  "return longest common prefix of the lines in region."
  (interactive "r")
  (let ((lines (split-string
                (buffer-substring-no-properties start end)
                "\n" t)))
    (message "%s" (length (cl-reduce
                           (lambda (s ss)
                             (if (string=
                                  ss "nonExist")
                                 s
                               (s-shared-start s ss)))
                           lines)))))

from sys import argv
import json

def read_line(l):
    nr, name, *verbs = l.strip().split(' ')
    if nr == '0':
        return None
    else:
        return {"paradigm": name, "verbs" : verbs}

def read_lines(ls):
    vs = []
    for l in ls:
        pl = read_line(l)
        if pl is None:
            pass
        else:
            vs.append(read_line(l))
    return vs

def make_irreg_besch_json(in_fp):
    with open(in_fp, mode='r', encoding="utf8") as fh:
        vs = read_lines(fh)
        return json.dumps({"verbs": vs},
                          ensure_ascii=False)

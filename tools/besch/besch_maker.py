# reads the paradigms.besch file, which contains the verb paradigm
# forms, and builds a .json with its data, which is then fed to the
# template using yasha.

from tools.tools import VERB_FORMS

from itertools import cycle, zip_longest
import json

# paradigm number + radical size + len(VERB_FORMS)
NUMBER_LINES_PARADIGM = 64


###
# auxiliary functions
def calculate_radical(infinitive, radical_size):
    if radical_size == 0:
        return "x"
    else:
        return infinitive[:radical_size]


def grouper(iterable, n, fillvalue=None):
    # thanks to https://stackoverflow.com/questions/434287/what-is-the-most-pythonic-way-to-iterate-over-a-list-in-chunks/434411#434411
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


###
# parsing functions
def read_forms(line, radical_size):
    def quote(string):
        return '"{}"'.format(string)

    def rm_radical(form, radical_size):
        return form[radical_size:]

    def quote_rm_radical(form, radical_size):
        return quote(rm_radical(form, radical_size))

    variants = line.split("|")
    lenvariants = len(variants)
    if lenvariants == 1:
        form = variants[0].strip()
        if form == "nonExist":
            return form
        else:
            return quote_rm_radical(form, radical_size)
    elif lenvariants == 2:
        return "vars {}".format(" ".join(map(lambda f: quote_rm_radical(f.strip(), radical_size),
                                             variants)))
    else:
        raise ValueError

def read_paradigm(plines, form_labels):
    paradigm = {}
    paradigm["number"] = plines[0].strip()
    radical_size = int(plines[1].strip())
    paradigm["name"] = plines[2].split("|")[0].strip()
    paradigm["suffix_size"] = len(paradigm["name"]) - radical_size
    for line in plines[2:]:
        paradigm[next(form_labels)] = read_forms(line, radical_size)
    paradigm["radical"] = calculate_radical(paradigm["name"], radical_size)
    return paradigm


def make_paradigms_json(fp):
    form_labels = cycle(VERB_FORMS)
    with open(fp, mode='r', encoding="utf8") as fh:
        ps = []
        for plines in grouper(fh, NUMBER_LINES_PARADIGM, ""):
            p = read_paradigm(plines, form_labels)
            ps.append(p)
            fh.readline()  # separator
        return json.dumps({"paradigms": ps},
                          ensure_ascii=False,
                          check_circular=False)

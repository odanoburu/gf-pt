import os
import jinja2

VERB_FORMS = ["infn", "ger", "part", "pres_ind_sg_p1", "pres_ind_sg_p2",
              "pres_ind_sg_p3", "pres_ind_pl_p1", "pres_ind_pl_p2",
              "pres_ind_pl_p3", "pres_sub_sg_p1", "pres_sub_sg_p2",
              "pres_sub_sg_p3", "pres_sub_pl_p1", "pres_sub_pl_p2",
              "pres_sub_pl_p3", "preti_ind_sg_p1", "preti_ind_sg_p2",
              "preti_ind_sg_p3", "preti_ind_pl_p1", "preti_ind_pl_p2",
              "preti_ind_pl_p3", "preti_sub_sg_p1", "preti_sub_sg_p2",
              "preti_sub_sg_p3", "preti_sub_pl_p1", "preti_sub_pl_p2",
              "preti_sub_pl_p3", "mqperf_sg_p1", "mqperf_sg_p2",
              "mqperf_sg_p3", "mqperf_pl_p1", "mqperf_pl_p2",
              "mqperf_pl_p3", "pretp_sg_p1", "pretp_sg_p2", "pretp_sg_p3",
              "pretp_pl_p1", "pretp_pl_p2", "pretp_pl_p3", "fut_ind_sg_p1",
              "fut_ind_sg_p2", "fut_ind_sg_p3", "fut_ind_pl_p1",
              "fut_ind_pl_p2", "fut_ind_pl_p3", "fut_sub_sg_p1",
              "fut_sub_sg_p2", "fut_sub_sg_p3", "fut_sub_pl_p1",
              "fut_sub_pl_p2", "fut_sub_pl_p3", "cond_sg_p1", "cond_sg_p2",
              "cond_sg_p3", "cond_pl_p1", "cond_pl_p2", "cond_pl_p3",
              "imper_sg_p2", "imper_sg_p3", "imper_pl_p1", "imper_pl_p2",
              "imper_pl_p3"]


def render_template(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)
